//vf2 actuator endurance code
//written by David S based off trash from John S
//  12/16/2021
//board Arduino Mega?
//using set up from John s

#include <ctype.h> //appears to be unused
#include <Arduino.h>
#include <Wire.h>              //used for I2C communication
#include <LiquidCrystal_I2C.h> // hopefully same lib as LCD in set up

const int enduranceButtonPin = 52; //possibly an enable pin for the program to run
const int openPin = 32;            //pin that will lead to the control board to open command
const int closePin = 33;           //pin that leads to control board to send close command
const int resetPin = 34;           //pin that leads to reset on control board?

const int openButtonPin = 42;  // this is push button to issue open command
const int closeButtonPin = 40; // pin to send open command

const int contactSensor = 2; // pin to read if there is a unit present in cut out

const int relay700Pin = 46; // resistor dialed to request 700V from lab power supply
const int relay225Pin = 48; // resistor dialed to request 225V from lab power supply

LiquidCrystal_I2C lcd(0x27, 20, 4); // initilization of LCD display NEED TO CHECK THAT I AM USING THE RIGHT LIBRARY

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(9600); // over usb

  pinMode(enduranceButtonPin, INPUT_PULLUP); //set up pins as input or output
  pinMode(openPin, OUTPUT);
  pinMode(closePin, OUTPUT);
  pinMode(resetPin, OUTPUT);
  pinMode(openButtonPin, INPUT_PULLUP);
  pinMode(closeButtonPin, INPUT_PULLUP);
  pinMode(contactSensor, INPUT);
  pinMode(relay700Pin, OUTPUT);
  pinMode(relay225Pin, OUTPUT);

  digitalWrite(relay700Pin, HIGH); //relay is active low. we need to boot up in high state to avoid non intended activation
  digitalWrite(relay225Pin, HIGH);

  lcd.init();
  Serial.begin(9600);
  lcd.backlight();
  lcd.clear();
}

void loop()
{
  // put your main code here, to run repeatedly:
  int onSwitch = 0;      // varible keeping track of run switch hardware
  int actuatorAlive = 1; //used to keep track if actuator is behaving
  int operations = 0;    // keep track of operations done while actuator is alive

  while (onSwitch == 0 && actuatorAlive == 1) //while onswitch is active and actuator is alive
  {
    actuatorAlive = testActuator();
    operations++;
    displayOps(operations);
    //delay(500); delay to keep lcd info displayed long enough to see
    onSwitch = digitalRead(enduranceButtonPin); // check that the run switch is still on this returns low/0 for on?
  }

  Serial.println("the program has finished");
  Serial.print("number of operations ");
  Serial.println(operations);
  displayOps(operations);
  while (1)
  {
    //wait forever until somebody finds the unit not making any noises
  }
}

void resetControlBoard() //function to reset vf2 control board to proactivly keep it from bricking
{
  digitalWrite(resetPin, HIGH);
  delay(50);
  Serial.println("resetting control");
  digitalWrite(resetPin, LOW);
  delay(1000);
}

int checkContinuity() // returns 0 if system contacts are open and 1 if system contacts are closed
{
  if (digitalRead(contactSensor) == 0) // this means system has closed
    return 1;
  else
    return 0;
}

void displayOps(int ops)
{
  lcd.clear(); //write ops on the screen
  lcd.setCursor(0, 0);
  lcd.print("number of ops "); // 14 is the last space
  lcd.setCursor(15, 0);
  lcd.print("     ");
  lcd.setCursor(14, 0);
  lcd.print(ops);

  Serial.print("number of operations "); // send ops to the computer
  Serial.println(ops);
  return;
}

int testActuator() //function that commands the actuator to move between its states and chec
{
  
  int didItClose = -1;
  int didItOpen = -1;
  resetControlBoard(); //supposed to be prevantative to keep the control board from bricking

  // this block should close actuator
  digitalWrite(relay700Pin, LOW); //prepare control board caps with 700 volts
  Serial.println("charging to 700v");
  delay(1800);                     // wait for caps to charge
  digitalWrite(relay700Pin, HIGH); // tell powersupply we do not need anything from it
  digitalWrite(closePin, HIGH);    // send commmand to fire actuator through hbridge
  delay(50);
  digitalWrite(closePin, LOW);
  delay(100);

  didItClose = checkContinuity(); //returns 1 for still good. 0 here means we expect continuity but are not finding it.

  //this block should open actuator
  digitalWrite(relay225Pin, LOW); //send the command to charge the control caps to 225v
  Serial.println("charging caps to 225v ");
  delay(1800);                     // wait for charge
  digitalWrite(relay225Pin, HIGH); //disconnect power supply
  digitalWrite(openPin, HIGH);
  delay(50);
  digitalWrite(openPin, LOW);
  delay(50);

  didItOpen = checkContinuity(); // should return 0 if things are as expected
  Serial.print("inside test acutator function. 1 for continuity 0 for not ");
  
  if (didItOpen == 0 && didItClose == 1)
  {
    Serial.println("both open and close detected as expected");
    return 1;
  }

  else
  {
    Serial.println("open or close not working");
    return 0;
  }
}
