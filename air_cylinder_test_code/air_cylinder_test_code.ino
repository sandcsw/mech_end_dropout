// this program allows easy and simple interactoin with the air cylinder in order to get its speed correct

#define triggerUp 7
#define triggerDown 6
#define cylinderUp 12
#define cylinderDown 5

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(115200);

  // set up for the switches to move the air cylinders and the air cylinder relay outputs.
  pinMode(cylinderUp, OUTPUT);
  pinMode(cylinderDown, OUTPUT);

  digitalWrite(cylinderUp, HIGH);   // the output is active low
  digitalWrite(cylinderDown, HIGH); // send low to move the air cylinder

  pinMode(triggerUp, INPUT_PULLUP);   // these are pulled up to 5v normally so when pressed they go low
  pinMode(triggerDown, INPUT_PULLUP); // digitalRead(triggerDown) == 0 will mean button is pressed
}

void loop()
{
  // put your main code here, to run repeatedly:

  //start up sequence for getting air cylinder pressurized on both ends
  int numPressCycle;
  int timeOn = 100;
  int timeOff = 100;
  while ((digitalRead(triggerDown) != 0)) // while the bottom button is not pressed
  {
    Serial.println("press limit switch for start up sequence");
  }

  for (numPressCycle = 0; numPressCycle < 2; numPressCycle++)
  {
    digitalWrite(cylinderDown, LOW);
    delay(timeOn);
    digitalWrite(cylinderDown, HIGH);
    delay(timeOn);
    digitalWrite(cylinderUp, LOW);
    delay(timeOn);
    digitalWrite(cylinderUp, HIGH);
    delay(timeOff);
  }

  //add code to loop over checking both switches for which one is active

  /*
  while(1)
  {
    
    digitalWrite(cylinderUp, HIGH); // set the opposing command to off 
    digitalWrite(cylinderDown, HIGH); // set the opposing command to off 
    while(digitalRead(triggerDown) == 0) // lower the air cylinder
    {
      Serial.println("move cylinder down");
      digitalWrite(cylinderUp, HIGH); // set the opposing command to off 
      delay(200); // wait for mechanical action to take place
      digitalWrite(cylinderDown, LOW); // do the command to lower the air cylinder
    }

    while(digitalRead(triggerUp) == 0) // raise the air cylinder
    {
      Serial.println("move cylinder up");
      digitalWrite(cylinderDown, HIGH); // set the opposing command to off 
      delay(200); // wait for mechanical action to take place
      digitalWrite(cylinderUp, LOW); // do the command to raise the air cylinder
    }
  }
*/
}
