// This example shows how to control the Jrk G2 over I2C by
// sending Set Target commands.
//
// The Jrk's input mode must be set to "Serial/I2C/USB".  The
// Jrk's device number must be set to its default value of 11.
//
// Please see https://github.com/pololu/jrk-g2-arduino for
// details on how to make the connections between the Arduino and
// the Jrk G2.




#include <JrkG2.h>

JrkG2I2C jrk;
int current = 0;


void setup()
{
  // Set up I2C.
  Wire.begin();
  Serial.begin(9600);
  pinMode(13,OUTPUT);
}

void loop()
{
  delay(1000);
  digitalWrite(13,HIGH); // FlaSh LeD oN bOArd arduino 
  jrk.setTarget(2648); // command motorcontroller for full forwards

  delay(1000); // wait 1 second
  Serial.print("current in mA: ");
  Serial.println(current); //return current used by linear actuator
  digitalWrite(13,LOW); // turn off arduinos LED
  jrk.setTarget(1448); // tell motorcontroller to go full reverse
}

void currentLog()
{
  current = jrk.getCurrent();
}

//set Target(2048) means stop the motor. there is alsoa stop command
//https://www.pololu.com/docs/0J73/5.1
// 2648 corresponds to full speed forward (100%), and a target of 1448 corresponds to full speed reverse (−100%).