// arduino new project on vscode
//david samuels
//using arduino uno
// include the library code:
#include <Wire.h>
#include <Adafruit_RGBLCDShield.h>     //lcd screen and buttons usae pins analog A4 and A5 on arduuino uno
#include <utility/Adafruit_MCP23017.h> // this is for the i2c button expansion chip on display board

Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield(); // this lcd varible has the functions inside of it to do the controls
void Estop();                                        // function prototype for emergency stop
void loadunit();                                     // function that lowers the arms to allow the unit to be put into the fixture

#define cylinderUp 12  //this extends the cylinder
#define cylinderDown 5 //this shrinks the cylinder
#define limSwTop 6
#define limSwBot 7

//#define estopPin 3 not in use anymore
#define sensePin 9

#define devToCutout 13 // this pin will activate putting the unit back into the cutout
#define handleUp 10    // will listen for a command to move the manual open handle up
#define handleDown 11  // will listen for a command to move th manual open handle down

// serial uses pins  0 1 internal routed
// i2c lib uses pins A4 A5 internal routed
// air cylinder control pins 12-BLUE-UP 5-GREEN-DOWN
// estop pin         3 YELLOW no longer used
// limit switch top 6 BLUE   switch is normally connected to 5v. when when pressed goes to gnd
// limit switch bot 7 BLUE   switch is normally connected to 5v. when when pressed goes to gnd
// continuity check for presence of unit in cutout on pin 4. when unit is closed in cut out the line is driven to ground. when unit open the output is high
// pin 2 and 3 are the only inturupt pins on the arduino uno.

void setup()
{

    pinMode(cylinderUp, OUTPUT);      // DIGITAL pin to activate air cylinder control  BLUE  // this signal moves clyinder up
    pinMode(cylinderDown, OUTPUT);    // DIGITAL pin to activate air cylinder control  GREEN   // this moves clyinder down
    digitalWrite(cylinderDown, HIGH); // having the pins be high is needed for saftey. The relay board is active low
    digitalWrite(cylinderUp, HIGH);   // we want the program to boot with the relay being off which means sending it a high command
    //pinMode(estopPin, INPUT_PULLUP);  //yellow wire estop button not in use anymore
    pinMode(limSwBot, INPUT_PULLUP); //limit switch 1 blue, when pressed they change to 0
    pinMode(limSwTop, INPUT_PULLUP); //limit switch 2 blue, when pressed they change to 0

    pinMode(devToCutout, INPUT_PULLUP);
    pinMode(handleUp, INPUT_PULLUP);
    pinMode(handleDown, INPUT_PULLUP);

    //attachInterrupt(digitalPinToInterrupt(3), Estop, HIGH); // normally connected to ground. when estop is hit it goes high to 5v triggering intturpt
    pinMode(sensePin, INPUT_PULLUP); // green and yellow wire on pin 9
    Serial.begin(115200);

    lcd.begin(16, 2);
    lcd.setBacklight(0x1);
}

void loop()
{

    int estoppin = -1;
    int limitswitch1 = -1;
    int limitswitch2 = -1;
    int operations = 0; // how many times the unit will run the drop out cycle
    while (1)
    {

        operations = menu();  //call the menu function to get how many droputs from user
        loadunit();           //get the test cage ready. unit must be inserted first
        testunit(operations); //the code which controls the test cage during the testing cycles

        //code for debug states of switches
        limitswitch1 = digitalRead(limSwBot);
        Serial.print("limitswitch bot :");
        Serial.println(limitswitch1);
        limitswitch2 = digitalRead(limSwTop);
        Serial.print("limitswitch top :");
        Serial.println(limitswitch2);

        //estoppin = digitalRead(3);
        //Serial.print(estoppin);
    }
    //*/
}

void Estop() // lcd functions dont work but digitalwrite does
{
    digitalWrite(cylinderDown, HIGH); // having the pins be high is needed for saftey. The relay board is active low
    digitalWrite(cylinderUp, HIGH);   // we want the program to boot with the relay being off which means sending it a high command
    //lcd.clear();
    //lcd.print("emergency stop triggered"); //this uses interrupts so it might not be a good idea to make in intturpt call in an intturpt
    while (1)
    {
        // program should stick here
    }
}

/*
    f() loadunit requires a unit to be already inserted into the test cage. This function will then place the arm into the bottom/lower starting position.
*/
void loadunit()
{
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("load func");
    delay(1000);
    // error handling
    if ((digitalRead(limSwBot) == 0) && (digitalRead(limSwTop) == 0)) // both limit switches are pressed which should never happen
    {
        digitalWrite(12, HIGH); // 12 is the raise cylinder up 5 is to lower the cylinder
        digitalWrite(5, HIGH);
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("error both limit");
        lcd.setCursor(0, 1);
        lcd.print("are pressed");
        while (1)
        {
        } // loop and do nothing forever
    }

    //main part of function
    if (digitalRead(limSwBot) == 0) // we are at the lowered state we do not need to do anything more
    {
        return;
    }

    if (digitalRead(limSwBot) != 0) // we are not at the bottom limit switch
    {
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("changing pos");

        digitalWrite(cylinderUp, HIGH);    // 12 is the raise cylinder up 5 is to lower the cylinder
        digitalWrite(cylinderDown, HIGH);  // these 2 digital write commands together freeze the aircylinder in place
        while (digitalRead(limSwBot) != 0) // while we are not at the bottom loading position
        {
            digitalWrite(cylinderUp, LOW); // send the lower command until we hit the bottom limit switch
        }
        digitalWrite(cylinderUp, HIGH); // halt the air cylinder at the bottom
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("at bottom");
    }
    return; // end of function
}

int contCheck() // returns 1 for continutiy and 0 for none
{
    int val = -1;
    val = digitalRead(sensePin);
    if (val == 1) //if 1 then pull up resistor takes over and there is no continuity
        return 0;

    return 1;
}

// this function is in charge of running the dropout test after the unit has been put in the loaded position
//requriments dropouts and unit in loaded position
void testunit(int dropouts)
{
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("starting  drop");

    while (dropouts > 0)
    {
        //the state machine goes: wait -> raise to top -> drop down -> wait for unit to drop out

        delay(5500); // wait x milli seconds <------------------- this will need tuning

        //the unit should be in the bottom position. we need to bring it up until it hits the top switch
        insertUnit();
        //might want to add a slight delay

        //the unit should be put in place and now we need to move our lever out of the way for the unit to swing back down
        moveArmDown();

        waitForDrop();

        dropouts = dropouts - 1;
        lcd.setCursor(0, 0);
        lcd.print("dropouts "); // need to add a special clearing function for part of led that updates all the time https://forum.arduino.cc/index.php?topic=75919.0
        lcd.setCursor(9, 0);    //set position to where numbers are written
        lcd.print("     ");     // write spaces to that spot efficivly clearing that area
        lcd.setCursor(9, 0);    // set cursor to where number are written again and rewrite in that blank space. this hellps with errors couting down from 110 etc
        lcd.print(dropouts);
        lcd.setCursor(0, 1);
        lcd.print("time ");
        lcd.print(dropouts * 5 / 60.0); // change this to reflect real cycle time to give an estimate of when the test will finsish
        lcd.print(" min");
        Serial.println("one operation complete");
    }

    //the test has finished so lower the arm down this is added to fix an error somewhere that still needs to be found
    Serial.print("cycle finished");
    /*   while (digitalRead(limSwBot) != 0)
        {
            digitalWrite(cylinderUp, LOW);    
        }
        digitalWrite(cylinderUp, HIGH); // stop the cylinder
        */
}

void insertUnit()
{
    //the unit should be in the bottom position. we need to bring it up until it hits the top switch
    Serial.println("moving up");
    while (digitalRead(limSwTop) != 0) //while the top switch is not pressed move up //this needs to be debounced
    {
        digitalWrite(cylinderDown, LOW); // command to raise air cylinder this is active low
    }
    digitalWrite(cylinderDown, HIGH); // the relay is active low so a high command shuts it off
    Serial.println("unit inserted");
}

void moveArmDown()
{
    Serial.println("moving down");
    while (digitalRead(limSwBot) != 0) //this is getting skipped???
    {

        Serial.println("inside part that is getting skipped moving arm out of the way"); // this has to stay in the code
        digitalWrite(cylinderUp, LOW);
    }
    digitalWrite(cylinderUp, HIGH); // stop the cylinder
    Serial.println("arm out of the way");
}

void waitForDrop() // this function sticks in the while loop pausing the program until the unit drops out
{
    while (contCheck() == 1)
    {
        Serial.println("waiting for unit to drop out");
    }
}

int menu()
{
    int dropouts = 0; // how many times we want to reset and drop out the unit
    uint8_t buttons = lcd.readButtons();

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("press select to");
    lcd.setCursor(0, 1);
    lcd.print("start");
    delay(500);
    while (buttons != BUTTON_SELECT) // wait until we hold the select button down
    {
        buttons = lcd.readButtons();
    }

    //add code to ask if user needs to pressureize the air cylinder
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("prime cylinder?");
    lcd.setCursor(0, 1);
    lcd.print("up-yes down-no");
    buttons = 0x0;
    while (buttons != BUTTON_UP && buttons != BUTTON_DOWN)
    {
        buttons = lcd.readButtons();
    }
    if (buttons == BUTTON_UP)
    {
        //add code here to prime air cylinder
        int numPressCycle;
        int timeOn = 100;
        int timeOff = 100;

        for (numPressCycle = 0; numPressCycle < 6; numPressCycle++)
        {
            digitalWrite(cylinderDown, LOW);
            delay(timeOn);
            digitalWrite(cylinderDown, HIGH);
            delay(timeOn);
            digitalWrite(cylinderUp, LOW);
            delay(timeOn);
            digitalWrite(cylinderUp, HIGH);
            delay(timeOff);
        }
    }

    // code to enable remote control of the fixture
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("remote control?");
    lcd.setCursor(0, 1);
    lcd.print("up-yes down-no");
    buttons = 0x0;
    while (buttons != BUTTON_UP && buttons != BUTTON_DOWN)
    {
        buttons = lcd.readButtons();
    }
    if (buttons == BUTTON_UP) //allow steves device to take control of the testers arm to reinsert the unit
    {
        //call remote control function. active low. will be normally be connected to 5v
        remoteControl();
    }

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("select cycles");
    delay(500);
    buttons = 0x0;
    do // menu code for selecting cycles
    {
        // wait for user input
        while (buttons == 0) // no press gives 0. when user presses command break out of loop and do it
        {
            buttons = lcd.readButtons();
        }
        if (buttons == BUTTON_UP) // different buttons add different amount of cycles into the machine
        {
            dropouts = dropouts + 10;
            delay(100); // time based debouncing.
        }

        else if (buttons == BUTTON_RIGHT)
        {
            dropouts = dropouts + 100;
            delay(100);
        }

        else if (buttons == BUTTON_DOWN)
        {
            dropouts = dropouts + 1;
            delay(100);
        }

        else if (buttons == BUTTON_LEFT)
        {
            dropouts = dropouts - 100;
            delay(100);
        }
        if (dropouts <= 0) // when erasing dropouts dont go below zero operations. that makes no sense to do
        {
            dropouts = 0;
            lcd.clear();
        }
        lcd.setCursor(0, 0);
        lcd.print("select cycles");
        lcd.setCursor(0, 1);
        lcd.print("# of cycles ");
        lcd.print(dropouts);
        buttons = lcd.readButtons();
    } while (buttons != BUTTON_SELECT); // press select to run the test

    // the user has selected the amount of ops. Start the test
    lcd.clear();

    return dropouts;
}

/*
this function will allow another MCU to drive the fixtures hardware (move the air cylinder and linear motor)
COMMANDS
driving pin 13 to ground will insert a unit into the cutout
driving pin 10 to ground will raise the linear motor pushing the units handle up
driving pin 11 to ground will lower the linear motor pulling the units handle down
*/
void remoteControl() //code that will control functions of drop out tester through its gpio pins so another arduino can sync up with the tester
{
    while (1)
    {
        while (digitalRead(handleUp) == 0)
        {
            Serial.println('\n');
            Serial.println("external command to push unit into cutout");
            //loadunit()
            insertUnit();
            moveArmDown();
        }

        while (digitalRead(handleUp) == 0)
        {
            //placeholder
        }
        while (digitalRead(handleDown) == 0)
        {
            //placeholder
        }
    }
}